function multiplication():void
{
    var input : HTMLInputElement = <HTMLInputElement>document.getElementById("input");
    var table : HTMLTableElement = <HTMLTableElement>document.getElementById("table");

    var count : number = 1;
    var num : number = parseFloat(input.value);

    if (isNaN(num))
    {
        alert ("you must enter Number");
    }
    else if(num<0)
    {
        alert("Number has to be greater than 0");
    }
    else if(num==0)
    {
        alert("Number can not be 0");
    }
    else if(!Number.isInteger(num))
    {
        alert("Enter only integers");
    }
    else
    {
    while(table.rows.length>1)
    {
        table.deleteRow(1);
    } 

    for(count = 1 ; count<=num ; count++)
    {
        var row : HTMLTableRowElement = table.insertRow(); // adds row 

        var cell : HTMLTableDataCellElement = row.insertCell(); //add cell which contain number entered by user
        var text:HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.style.textAlign="center";
        text.value=num.toString();
        cell.appendChild(text);

        var cell : HTMLTableDataCellElement = row.insertCell(); //add * cell in row
        var mul:string = "*";
        var text:HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.style.textAlign="center";
        text.value=mul.toString();
        cell.appendChild(text);

        var cell : HTMLTableDataCellElement = row.insertCell(); //add count in row 
        var text:HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.style.textAlign="center";
        text.value=count.toString();
        cell.appendChild(text);

        var cell : HTMLTableDataCellElement = row.insertCell(); //adds = sign in row cell
        var mul:string = "=";
        var text:HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.style.textAlign="center";
        text.value=mul.toString();
        cell.appendChild(text);

        var cell : HTMLTableDataCellElement = row.insertCell(); // adds multiplication in row 
        var text:HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.style.textAlign="center";
        text.value=(num*count).toString();
        cell.appendChild(text);
    }
}
}