function chk() {
    var num1 = document.getElementById("x1");
    var num2 = document.getElementById("y1");
    var num3 = document.getElementById("x2");
    var num4 = document.getElementById("y2");
    var num5 = document.getElementById("x3");
    var num6 = document.getElementById("y3");
    var num7 = document.getElementById("x");
    var num8 = document.getElementById("y");
    var tmp1 = parseFloat(num1.value); //x1
    var tmp2 = parseFloat(num2.value); //y1
    var tmp3 = parseFloat(num3.value); //x2
    var tmp4 = parseFloat(num4.value); //y2
    var tmp5 = parseFloat(num5.value); //x3
    var tmp6 = parseFloat(num6.value); //y3
    var tmp7 = parseFloat(num7.value); //x
    var tmp8 = parseFloat(num8.value); //y
    if (isNaN(tmp1) || isNaN(tmp1) || isNaN(tmp2) || isNaN(tmp3) || isNaN(tmp4) || isNaN(tmp5) || isNaN(tmp6) || isNaN(tmp7) || isNaN(tmp8)) {
        alert("Value Must be Number");
    }
    else if (num1.value.length == 0 || num2.value.length == 0 || num3.value.length == 0 || num4.value.length == 0 || num5.value.length == 0 || num6.value.length == 0 || num7.value.length == 0 || num8.value.length == 0) {
        alert("Can not be NULL");
    }
    else {
        var sum; //created sum variables to calculate triangle areas
        var sum1;
        var sum2;
        var sum3;
        var sum4;
        sum1 = ((tmp1 * (tmp4 - tmp6)) + (tmp3 * (tmp6 - tmp2)) + (tmp5 * (tmp2 - tmp4))) / 2; //A(ABC)
        sum2 = ((tmp7 * (tmp2 - tmp4)) + (tmp1 * (tmp4 - tmp8)) + (tmp3 * (tmp8 - tmp2))) / 2; //A(PAB)
        sum3 = ((tmp7 * (tmp3 - tmp6)) + (tmp3 * (tmp6 - tmp8)) + (tmp5 * (tmp8 - tmp3))) / 2; //A(PBC)
        sum4 = ((tmp7 * (tmp2 - tmp6)) + (tmp1 * (tmp6 - tmp8)) + (tmp5 * (tmp8 - tmp2))) / 2; //A(PAC)
        sum = sum2 + sum3 + sum4; //so sum of all the areas
        if ((Math.abs(sum1 - sum)) < 0.0000001) //checks if the point is in the triangle or not 
         {
            document.getElementById("ans").innerHTML = "The point lies inside the Triangle";
        }
        else {
            document.getElementById("ans").innerHTML = "The point lies Outside the Triangle";
        }
    }
}
//# sourceMappingURL=check_point.js.map